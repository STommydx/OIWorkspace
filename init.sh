#!/usr/bin/env bash

for prb in A B C D E F G
do
	cp src/template/solve.cpp src/$prb.cpp
done

mkdir -p build
pushd build
cmake ..
make -j4
popd
