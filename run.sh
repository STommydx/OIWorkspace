#!/usr/bin/env bash

function green {
    echo -e `tput setaf 2`"$@"`tput sgr0` >&2
}

function red {
    echo -e `tput setaf 1`"$@"`tput sgr0` >&2
}

function yellow {
    echo -e `tput setaf 3`"$@"`tput sgr0` >&2
}

green "[*] Building..."
pushd build >&2
if ! make -j4 $(basename $0).out < /dev/null >&2; then
    red "[!] Build failure, aborting..."
    exit 1
fi
popd >&2

green "[*] Running Problem $(basename $0)..."

if build/$(basename $0).out; then
green "[*] Program exit with code $?"
else
red "[!] Program exit with code $?"
fi

extra_tokens=$(cat | wc -w)
if [[ $extra_tokens != "0" ]]; then
    yellow "[!] $extra_tokens extra tokens found"
fi
