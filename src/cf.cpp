#include <iostream>
#include <vector>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <regex>
#include <chrono>

class COLOR {
	public:
		static const std::string RST;
		static const std::string RED;
		static const std::string GRN;
};
const std::string COLOR::RST{"\x1B[0m"};
const std::string COLOR::RED{"\x1B[31m"};
const std::string COLOR::GRN{"\x1B[32m"};

void _failExecExit() {
	std::cerr << "ERROR: Fail to exec program." << std::endl;
	exit(1);
}

void _failForkExit() {
	std::cerr << "ERROR: Fail to fork program." << std::endl;
	exit(1);
}


int main(int argc, char *const argv[]) {
	using namespace std;
	// cerr << "No. of args: " << argc << endl;
	// for (int i=0;i<argc;i++) cout << "Arg " << i << ": " << argv[i] << endl;

	bool runOption = false;
	bool testOption = false;
	bool exitOnFirstWA = false;

	int opt;
	while ((opt = getopt(argc, argv, "rte")) != -1) {
		switch (opt) {
			case 'r':
				runOption = true;
				break;
			case 't':
				testOption = true;
				break;
			case 'e':
				exitOnFirstWA = true;
				break;
		}
	}

	vector<string> problemList;
	for (; optind < argc; optind++) {
		problemList.emplace_back(argv[optind]);
	}
	if (problemList.empty()) {
		cout << "Problem List empty! Using default list." << endl;
		problemList.push_back("A");
		problemList.push_back("B");
		problemList.push_back("C");
		problemList.push_back("D");
		problemList.push_back("E");
		problemList.push_back("F");
	}

	for (const string &problem : problemList) {
		cout << "Processing problem " << problem << "......" << endl;

		if (runOption) {
			cout << "Running problem " << problem << "......" << endl;
			if (testOption) {
				vector<string> testFiles;
				regex re(problem + "\\.([0-9]+)\\.in");
				DIR *directory = opendir(".");
				if (directory) {
					dirent *currentDir;
					while ((currentDir = readdir(directory))) {
						cmatch matchResult;
						if (regex_match(currentDir->d_name, matchResult, re)) {
							testFiles.emplace_back(matchResult[1].str());
						}
					}
					closedir(directory);
				}
				for (const string &test : testFiles) {
					cout << "Test " << test << ": " << flush;
					string testInName = string{"./"} + problem + "." + test + ".in";
					string testOutName = string{"./"} + problem + "." + test + ".out";
					string testAnsName = string{"./"} + problem + "." + test + ".ans";
					string testDiffName = string{"./"} + problem + "." + test + ".diff";

					auto printVerdict = [&] (string verdict, string color, int exitCode, long time) {
						cout << color << verdict << COLOR::RST;
						cout << " (Exit code: " << exitCode << ", Time: " << time << "ms)" << endl;
					};

					auto startTime = chrono::high_resolution_clock::now();
					if (pid_t pid = fork(); pid == 0) {
						int readfd = open(testInName.c_str(), O_RDONLY);
						int writefd = open(testOutName.c_str(), O_WRONLY | O_CREAT | O_TRUNC , S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
						close(0); close(1);
						dup2(readfd, 0);
						dup2(writefd, 1);
						const char *path = (string("./") + problem).c_str();
						execl(path, path);
						_failExecExit();
					} else if (pid == -1) {
						_failForkExit();
					}
					int statuscode;
					wait(&statuscode);
					auto endTime = chrono::high_resolution_clock::now();
					auto timeDiff = chrono::duration_cast<chrono::milliseconds>(endTime - startTime);
					if (statuscode) {
						printVerdict("RE", COLOR::RED, statuscode, timeDiff.count());
						if (exitOnFirstWA) return 0;
					} else {
						int diffstatus;
						if (pid_t pid = fork(); pid == 0) {
							int difffd = open(testDiffName.c_str(), O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
							close(1);
							dup2(difffd, 1);
							execlp("diff", "diff", testOutName.c_str(), testAnsName.c_str());
							_failExecExit();
						} else if (pid == -1) {
							_failForkExit();
						}
						wait(&diffstatus);
						if (diffstatus) {
							printVerdict("WA", COLOR::RED, statuscode, timeDiff.count());
							if (exitOnFirstWA) {
								auto cat = [&] (auto str) {
									if (pid_t pid = fork(); pid == 0) {
										execlp("cat", "cat", "--", str);
										_failExecExit();
									} else if (pid == -1) {
										_failForkExit();
									}
									int statuscode;
									wait(&statuscode);
									return statuscode;
								};
								cout << "=== Input ===" << endl;
								cat(testInName.c_str());
								cout << "=== Output ===" << endl;
								cat(testOutName.c_str());
								cout << "=== Expected ===" << endl;
								cat(testAnsName.c_str());
								return 0;
							}
						} else {
							printVerdict("AC", COLOR::GRN, statuscode, timeDiff.count());
						}
					}
				}
			} else {
				auto startTime = chrono::high_resolution_clock::now();
				if (pid_t pid = fork(); pid == 0) {
					const char *path = (string{"./"} + problem).c_str();
					execl(path, path);
					_failExecExit();
				} else if (pid == -1) {
					_failForkExit();
				}
				int statuscode;
				wait(&statuscode);
				auto endTime = chrono::high_resolution_clock::now();
				auto timeDiff = chrono::duration_cast<chrono::milliseconds>(endTime - startTime);
				cout << "Problem " << problem << " exit with exit code: " << statuscode << endl;
				cout << "Time elapsed: " << timeDiff.count() << "ms" << endl;
			}
		}
	}


	return 0;
}
