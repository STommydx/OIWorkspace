template <class T = int> constexpr T INFTY = std::numeric_limits<T>::has_infinity ? std::numeric_limits<T>::infinity() : std::numeric_limits<T>::max();

// Combine Operators {{{
namespace Combine {
	template <class T> struct Sum {
		constexpr T operator()(const T &lhs, const T &rhs) { return lhs + rhs; }
		constexpr const T def() { return T(); }
	};
	template <class T> struct Max {
		constexpr T operator()(const T &lhs, const T &rhs) { return max(lhs, rhs); }
		constexpr const T def() { return -INFTY<T>; }
	};
	template <class T> struct Min {
		constexpr T operator()(const T &lhs, const T &rhs) { return min(lhs, rhs); }
		constexpr const T def() { return INFTY<T>; }
	};
	template <class T> struct Gcd {
		constexpr T operator()(const T &lhs, const T &rhs) { return gcd(lhs, rhs); }
		constexpr const T def() { return T(); }
	};
}
// }}}

// Update Operators {{{
namespace Update {
	template <class T, class U = T, bool summation = false> struct Assign {
		constexpr T &operator()(T &lhs, const U &rhs, int len = 1) { return summation ? lhs = rhs * len : lhs = rhs; }
		constexpr U &upLazy(U &lhs, const U &rhs) { return lhs = rhs; }
		constexpr const U udef() { return INFTY<U>; }
	};
	template <class T, class U = T, bool summation = false> struct Add {
		constexpr T &operator()(T &lhs, const U &rhs, int len = 1) { return summation ? lhs += rhs * len : lhs += rhs; }
		constexpr U &upLazy(U &lhs, const U &rhs) { return lhs += rhs; }
		constexpr const U udef() { return U(); }
	};
}
// }}}

// vim: fdm=marker

