#include <vector>
#include <algorithm>
using namespace std;

#include "operators.h"

// 1D Sparse Table {{{
template <class T, class Combinator=Combine::Max<T>>
class SparseTable {
	static inline Combinator op;
	int n, lgn = 1;
	vector<vector<T>> dp;
	vector<int> myLog;

public:
	SparseTable(const vector<T> &init) : n(init.size()), myLog(n + 1) {
		while ((1 << lgn) <= n) lgn++;
		dp.resize(lgn, vector<T>(n));
		dp[0] = init;
		for (int j = 1; j < lgn; j++) {
			for (int i = 0; i + (1 << j) <= n; i++) {
				dp[j][i] = op(dp[j - 1][i], dp[j - 1][i + (1 << (j - 1))]);
			}
		}
		myLog[1] = 0;
		for (int i = 2; i <= n; i++)
			myLog[i] = myLog[i / 2] + 1;
	}

	T query(int l, int r) {
		int j = myLog[r - l + 1];
		return op(dp[j][l], dp[j][r - (1 << j) + 1]);
	}

};
// }}}

// 2D Sparse Table {{{
template <class T, class Combinator=Combine::Max<T>>
class TDSparseTable {
	static inline Combinator op;
	int n, m;
	int lgn = 1, lgm = 1;
	vector<vector<vector<vector<T>>>> dp;
	vector<int> myLog;

public:
	TDSparseTable(const vector<vector<T>> &init) : n(init.size()), m(init[0].size()), myLog(max(n, m) + 1) {
		while ((1 << lgn) <= n) lgn++;
		while ((1 << lgm) <= m) lgm++;
		dp.resize(lgn, vector<vector<vector<T>>>(lgm, vector<vector<T>>(n, vector<T>(m))));
		dp[0][0] = init;
		for (int lj = 1; lj < lgm; lj++)
			for (int i = 0; i < n; i++)
				for (int j = 0; j + (1 << lj) - 1 < m; j++)
					dp[0][lj][i][j] = op(dp[0][lj - 1][i][j], dp[0][lj - 1][i][j + (1 << (lj - 1))]);
		for (int li = 1; li < lgn; li++)
			for (int lj = 0; lj < lgm; lj++)
				for (int i = 0; i + (1 << li) - 1 < n; i++)
					for (int j = 0; j + (1 << lj) - 1 < m; j++)
						dp[li][lj][i][j] = op(dp[li - 1][lj][i][j], dp[li - 1][lj][i + (1 << (li - 1))][j]);
		myLog[1] = 0;
		for (int i = 2; i <= n || i <= m; i++)
			myLog[i] = myLog[i / 2] + 1;
	}

	T query(int lx, int ly, int rx, int ry) {
		int li = myLog[rx - lx + 1];
		int lj = myLog[ry - ly + 1];
		return op(op(dp[li][lj][lx][ly], dp[li][lj][lx][ry - (1 << lj) + 1]), op(dp[li][lj][rx - (1 << li) + 1][ly], dp[li][lj][rx - (1 << li) + 1][ry - (1 << lj) + 1]));
	}

};
// }}}

// vim: fdm=marker
