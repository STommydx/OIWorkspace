#include <utility>
using namespace std;

// Magic Zip Template {{{ 
template <class T, class U>
struct ZipIterator : pair<T, U> {
	using difference_type = typename T::difference_type;
	using value_type = pair<typename T::value_type, typename U::value_type>;
	using pointer = pair<typename T::pointer, typename U::pointer>;
	using reference = pair<typename T::reference, typename U::reference>;
	using iterator_category = typename T::iterator_category;

	template <class... Args> ZipIterator(Args &&... args) : pair<T, U>(forward<Args>(args)...) {}
	ZipIterator(const ZipIterator &) = default;
	ZipIterator(ZipIterator &&) = default;
	ZipIterator &operator=(const ZipIterator &) = default;
	ZipIterator &operator=(ZipIterator &&) = default;

	reference operator*() { return make_pair(ref(*(this->first)), ref(*(this->second))); }
	ZipIterator &operator++() {
		++this->first; ++this->second;
		return *this;
	}
	ZipIterator operator++(int) {
		ZipIterator now(*this);
		++(*this);
		return now;
	}
	ZipIterator &operator--() {
		++this->first; ++this->second;
		return *this;
	}
	ZipIterator operator--(int) {
		ZipIterator now(*this);
		++(*this);
		return now;
	}
	ZipIterator &operator+=(difference_type n) {
		this->first += n; this->second += n;
		return *this;
	}
	ZipIterator operator+(difference_type n) const {
		ZipIterator tmp(*this);
		return tmp += n;
	}
	ZipIterator &operator-=(difference_type n) {
		this->first -= n; this->second -= n;
		return *this;
	}
	ZipIterator operator-(difference_type n) const {
		ZipIterator tmp(*this);
		return tmp -= n;
	}
	difference_type operator-(const ZipIterator &rhs) const {
		return this->first - rhs.first;
	}
	reference operator[](difference_type n) {
		return *(*this + n);
	}

};
template <class T, class U>
struct ZipContainer {
	using size_type = typename T::size_type;
	using value_type = pair<typename T::value_type, typename U::value_type>;
	using reference = pair<typename T::reference, typename U::reference>;
	using iterator = ZipIterator<typename T::iterator, typename U::iterator>;
	using reverse_iterator = ZipIterator<typename T::reverse_iterator, typename U::reverse_iterator>;

	T &first;
	U &second;
	ZipContainer(T &first, U &second) : first(first), second(second) {}
	iterator begin() { return iterator{first.begin(), second.begin()}; }
	reverse_iterator rbegin() { return reverse_iterator{first.rbegin(), second.rbegin()}; }
	iterator end() { return iterator{first.end(), second.end()}; }
	reverse_iterator rend() { return reverse_iterator{first.rend(), second.rend()}; }
	reference front() { return make_pair(ref(first.front()), ref(second.front())); }
	reference back() { return make_pair(ref(first.back()), ref(second.back())); }
	reference operator[](size_type pos) { return make_pair(ref(first[pos]), ref(second[pos])); }
	bool empty() const { return first.empty(); }
	size_type size() const { return first.size(); }
	void clear() { first.clear(); second.clear(); }
	void push_back(const value_type &value) {
		first.push_back(value.first);
		second.push_back(value.second);
	}
	void pop_back() {
		first.pop_back();
		second.pop_back();
	}
};

template <class T, class U>
ZipContainer<T, U> zip(T &first, U &second) { return ZipContainer<T, U>(first, second); }

namespace std {
	template <class T1, class U1, class T2, class U2>
	enable_if_t<is_same<remove_reference_t<T1>, remove_reference_t<T2>>::value && is_same<remove_reference_t<U1>, remove_reference_t<U2>>::value, bool>
	operator<(const pair<T1, U1> &lhs, const pair<T2, U2> &rhs) {
		if (lhs.first < rhs.first) return true;
		if (rhs.first < lhs.first) return false;
		return lhs.second < rhs.second;
	}

	template <class T1, class U1, class T2, class U2>
	enable_if_t<is_same<remove_reference_t<T1>, remove_reference_t<T2>>::value && is_same<remove_reference_t<U1>, remove_reference_t<U2>>::value, bool>
	operator==(const pair<T1, U1> &lhs, const pair<T2, U2> &rhs) {
		return lhs.first == rhs.first && lhs.second == rhs.second;
	}
	
	template <class T, class U>
	void swap(const pair<T&, U&> &lhs, const pair<T&, U&> &rhs) {
		swap(lhs.first, rhs.first);
		swap(lhs.second, rhs.second);
	}
}
// }}}

// vim: fdm=marker
