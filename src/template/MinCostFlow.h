#include <vector>
#include <queue>
#include <algorithm>

// Min-cost Maximum Flow {{{
class MinCostFlow{
	private:
		static constexpr int INF = 0x3f3f3f3f;
		int n, s, t;

		struct Edge {
			int from, to, cap, wei;
			Edge *rev;

			Edge(int u, int v, int c, int w) :
		 		from(u), to(v), cap(c), wei(w), rev(nullptr) {}
		};

		std::vector<std::vector<Edge *>> g;

		std::vector<int> dist, vis;
		std::vector<Edge *> par;

		void spfaSP() {
			fill(dist.begin(), dist.end(), INF);
			fill(vis.begin(), vis.end(), 0);
			fill(par.begin(), par.end(), nullptr);

			std::queue<int> q;
			dist[s] = 0; q.push(s);
			while(!q.empty()) {
				int u = q.front(); q.pop(); vis[u] = 0;
				for (Edge *e: g[u]) {
					if (e->cap <= 0) continue;
					int v = e->to, l = e->wei;
					if (dist[u] + l < dist[v]) {
						dist[v] = dist[u] + l;
						par[v] = e;
						if (!vis[v]) q.push(v), vis[v] = 1;
					}
				}
			}
		}

		int augmentFlow() {
			int curFlow = INF;
			for (int u = t; u != s; u = par[u]->from)
				curFlow = std::min(curFlow, par[u]->cap);
			for (int u = t; u != s; u = par[u]->from) {
				par[u]->cap -= curFlow;
				par[u]->rev->cap += curFlow;
			}
			return curFlow;
		}

	public:
		MinCostFlow(int num, int source, int dest) : 
			n(num), s(source), t(dest), g(n), dist(n), vis(n), par(n) {}

		~MinCostFlow() {
			for (int i=0;i<n;i++) for (Edge *e: g[i]) delete e;
		}

		void addEdge(int u, int v, int c, int w) {
			Edge *uv = new Edge(u, v, c, w);
			Edge *vu = new Edge(v, u, 0, -w);
			uv->rev = vu; vu->rev = uv;
			g[u].push_back(uv); g[v].push_back(vu);
		}

		void setKFlow(int k) {
			int newSource = n++;
			g.resize(n); dist.resize(n); vis.resize(n); par.resize(n);
			addEdge(newSource, s, k, 0);
			s = newSource;
		}

		std::pair<int, int> minCostFlow() {
			int mxFlow = 0;
			int cost = 0;
			for (;;) {
				spfaSP();
				if (dist[t] == INF) break;
				int fo = augmentFlow();
				mxFlow += fo;
				cost += dist[t] * fo;
			}
			return {cost, mxFlow};
		}

		int getSource() { return s; }
		int getSink() { return t; }

};
constexpr int MinCostFlow::INF;
// }}}

// vim: fdm=marker
