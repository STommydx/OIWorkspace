#include <vector>
using namespace std;

// DSU Template {{{
class DSU : vector<int> {
public:
	DSU(int n) : vector<int>(n, -1) {}
	int fi(int x) { return (*this)[x] >= 0 ? (*this)[x] = fi((*this)[x]) : x; }
	int sizeOf(int x) { return -(*this)[fi(x)]; }
	int un(int u, int v) {
		int gu = fi(u), gv = fi(v);
		if (gu == gv) return -1;
		if ((*this)[gu] < (*this)[gv]) ::swap(gu, gv);
		(*this)[gv] += (*this)[gu];
		(*this)[gu] = gv;
		return gv;
	}
};
// }}}

// vim: fdm=marker
