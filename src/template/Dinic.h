#include <vector>
#include <queue>
#include <algorithm>
#include <limits>

// Dinic Maximum Flow {{{
template <class T>
class Dinic {

private:

	struct edge {
		int to; T cap;
		edge *rev;
		edge(int v, T c) : to(v), cap(c), rev(nullptr) {}
	};

	static constexpr T INF = INFTY<T>;
	static constexpr int DINF = INFTY<int>;

	int n, s, t;
	vector<vector<edge*>> g;

public:

	Dinic(int n, int s, int t) : n(n), s(s), t(t), g(n) {}
	~Dinic() { for (auto v : g) for (edge *e : v) delete e; }

	void addEdge(int u, int v, T c) {
		edge *in = new edge(v, c);
		edge *out = new edge(u, 0);
		g[u].push_back(in); g[v].push_back(out);
		in->rev = out; out->rev = in;
	}

	T maxFlow() {
		T mxf = 0;
		vector<int> dist(n, DINF), vis(n);
		function<T(int, T)> dfs = [&] (int u, T fo) -> T {
			if (u == t) return fo;
			for (;vis[u]<int(g[u].size());vis[u]++) {
				edge *e = g[u][vis[u]];
				int v = e->to;
				if (dist[v] != dist[u] + 1 || e->cap <= 0) continue;
				T foo = dfs(v, min(fo, e->cap));
				if (foo > 0) {
					e->cap -= foo;
					e->rev->cap += foo;
					return foo;
				}
			}
			return 0;
		};
		for (;;) {
			for (int i=0;i<n;i++) dist[i] = DINF, vis[i] = 0;
			queue<int> q; q.push(s); dist[s] = 0;
			while (!q.empty()) {
				int u = q.front(); q.pop();
				if (u == t) break;
				for (edge *e: g[u]) {
					int v = e->to;
					if (e->cap > 0 && dist[v] == DINF) {
						dist[v] = dist[u] + 1;
						q.push(v);
					}
				}
			}
			if (dist[t] == DINF) break;
			while (T fo = dfs(s, INF))
				mxf += fo;
		}
		return mxf;
	}

};
template <class T> constexpr T Dinic<T>::INF;
template <class T> constexpr int Dinic<T>::DINF;
// }}}

// vim: fdm=marker
