#include <vector>
#include <limits>
#include "operators.h"

// Classic Segment Tree {{{
template <class T, class U = T, class Combinator = Combine::Min<T>, class Updater = Update::Assign<T>>
class SegmentTree {
	static inline Combinator combinator;
	static inline Updater updater;
	const int n;
	vector<T> tree;

public:
	explicit SegmentTree(const vector<T> &init) : n(init.size()), tree(n) {
		copy(init.begin(), init.end(), back_inserter(tree));
		for (int i = n - 1; i > 0; i--) tree[i] = combinator(tree[i << 1], tree[i << 1 | 1]);
	}
	explicit SegmentTree(int n, const T &init = combinator.def()) : SegmentTree(vector<T>(n, init)) {}

	void modify(int p, const U &val) {
		for (updater(tree[p += n], val); p >>= 1; ) tree[p] = combinator(tree[p << 1], tree[p << 1 | 1]);
	}

	T query(int l, int r) {
		T resl = combinator.def(), resr = combinator.def();
		for (l += n, r += n + 1; l < r; l >>= 1, r >>= 1) {
			if (l & 1) resl = combinator(resl, tree[l++]);
			if (r & 1) resr = combinator(tree[--r], resr);
		}
		return combinator(resl, resr);
	}

};
// }}}

// Range Update Segment Tree {{{
template <class T, class U = T, class Combinator = Combine::Min<T>, class Updater = Update::Assign<T>>
class RangeSegmentTree {
	static inline Combinator combinator;
	static inline Updater updater;
	const int n;
	vector<T> tree;

public:
	explicit RangeSegmentTree(const vector<T> &init) : n(init.size()), tree(init.size()) {
		copy(init.begin(), init.end(), back_inserter(tree));
		for (int i = n - 1; i > 0; i--) tree[i] = combinator(tree[i << 1], tree[i << 1 | 1]);
	}
	explicit RangeSegmentTree(int n, const T &init = combinator.def()) : RangeSegmentTree(vector<T>(n, init)) {}

	T query(int p) {
		T res = combinator.def();
		for (p += n; p > 0; p >>= 1) res = combinator(res, tree[p]);
		return res;
	}

	void modify(int l, int r, const U &val) {
		for (l += n, r += n + 1; l < r; l >>= 1, r >>= 1) {
			if (l & 1) updater(tree[l++], val);
			if (r & 1) updater(tree[--r], val);
		}
	}

};
// }}}

// Lazy Segment Tree {{{
template <class T, class U = T, class Combinator = Combine::Min<T>, class Updater = Update::Assign<T>>
class LazySegmentTree {
	static inline Combinator combinator;
	static inline Updater updater;
	const int n;
	int h;
	vector<T> tree;
	vector<U> lazy;
	vector<bool> tag;

	void calc(int p, int len) {
		tree[p] = combinator(tree[p << 1], tree[p << 1 | 1]);
		if (tag[p]) updater(tree[p], lazy[p], len);
	}

	void apply(int p, const U &val, int len) {
		updater(tree[p], val, len);
		if (p < n) updater.upLazy(lazy[p], val), tag[p] = true;
	}

	void build(int p) {
		int len = 2;
		for (p += n; p >>= 1; len <<= 1) calc(p, len);
	}

	void push(int p) {
		int s = h, len = 1 << (h - 1);
		for (p += n; s > 0; s--, len >>= 1) {
			int i = p >> s;
			if (tag[i]) {
				apply(i << 1, lazy[i], len);
				apply(i << 1 | 1, lazy[i], len);
				lazy[i] = updater.udef(), tag[i] = false;
			}
		}
	}

public:
	explicit LazySegmentTree(const vector<T> &init) : n(init.size()), h(0), tree(n), lazy(n + n, updater.udef()), tag(n + n) {
		copy(init.begin(), init.end(), back_inserter(tree));
		while ((1 << h) <= n) h++;
		for (int i = n - 1; i > 0; i--) tree[i] = combinator(tree[i << 1], tree[i << 1 | 1]);
	}
	explicit LazySegmentTree(int n, const T &init = combinator.def()) : LazySegmentTree(vector<T>(n, init)) {}

	void modify(int l, int r, const U &val) {
		push(l); push(r);
		int l0 = l, r0 = r, len = 1;
		for (l += n, r += n + 1; l < r; l >>= 1, r >>= 1, len <<= 1) {
			if (l & 1) apply(l++, val, len);
			if (r & 1) apply(--r, val, len);
		}
		build(l0); build(r0);
	}

	T query(int l, int r) {
		push(l); push(r);
		T res = combinator.def();
		for (l += n, r += n + 1; l < r; l >>= 1, r >>= 1) {
			if (l & 1) res = combinator(res, tree[l++]);
			if (r & 1) res = combinator(tree[--r], res);
		}
		return res;
	}

};
// }}}

// Dynamic Segment Tree {{{
template <class T, class U = T, class Combinator = Combine::Min<T>, class Updater = Update::Assign<T>, class SegIndex = int>
class DynamicSegmentTree {
private:

	struct SegNode;

	#ifndef LOCAL_JUDGE
		using SegPointer = SegNode *;
	#else
		using SegPointer = unique_ptr<SegNode>;
	#endif

	struct SegNode {
		T tree;
		U lazy;
		SegPointer lchi, rchi;
	};

	static inline Combinator combinator;
	static inline Updater updater;
	SegIndex n;
	SegPointer root = nullptr;
	const T defVal;

	void refreshNode(SegPointer &u, SegIndex lo, SegIndex hi) {
		if (u) return;
		T nodeValue = combinator.def();
		updater(nodeValue, defVal, hi - lo + 1);
		u = SegPointer{new SegNode{nodeValue, updater.udef(), nullptr, nullptr}};
	}

	void apply(const U &val, SegPointer &u, SegIndex lo, SegIndex hi) {
		refreshNode(u, lo, hi);
		updater(u->tree, val, hi - lo + 1);
		updater.upLazy(u->lazy, val);
	}

	void push(SegPointer &u, SegIndex lo, SegIndex hi) {
		if (u->lazy != updater.udef()) {
			SegIndex mi = lo + (hi - lo) / 2;
			apply(u->lazy, u->lchi, lo, mi);
			apply(u->lazy, u->rchi, mi + 1, hi);
			u->lazy = updater.udef();
		}
	}

	void build(SegPointer &u, SegIndex lo, SegIndex hi, const std::vector<T> &init) {
		refreshNode(u, lo, hi);
		if (lo == hi) {
			u->tree = init[lo];
		} else {
			SegIndex mi = lo + (hi - lo) / 2;
			build(u->lchi, lo, mi, init);
			build(u->rchi, mi + 1, hi, init);
			u->tree = combinator(u->lchi->tree, u->rchi->tree);
		}
	}

	void modify(SegIndex l, SegIndex r, const U &val, SegPointer &u, SegIndex lo, SegIndex hi) {
		refreshNode(u, lo, hi);
		if (r < lo || l > hi || l > r) return;
		if (l <= lo && hi <= r) {
			apply(val, u, lo, hi);
			return;
		}
		push(u, lo, hi);
		SegIndex mi = lo + (hi - lo) / 2;
		modify(l, r, val, u->lchi, lo, mi);
		modify(l, r, val, u->rchi, mi + 1, hi);
		u->tree = combinator(u->lchi->tree, u->rchi->tree);
	}

	T query(SegIndex l, SegIndex r, SegPointer &u, SegIndex lo, SegIndex hi) {
		refreshNode(u, lo, hi);
		if (r < lo || l > hi || l > r) return combinator.def();
		if (l <= lo && hi <= r) {
			return u->tree;
		}
		push(u, lo, hi);
		SegIndex mi = lo + (hi - lo) / 2;
		T &&lres = query(l, r, u->lchi, lo, mi);
		T &&rres = query(l, r, u->rchi, mi + 1, hi);
		return combinator(lres, rres);
	}

public:
	explicit DynamicSegmentTree(const vector<T> &init) : n(init.size()) {
		build(root, 0, n - 1, init);
	}
	explicit DynamicSegmentTree(SegIndex n, const T &def = combinator.def()) : n(n), defVal(def) {}

	void modify(SegIndex l, SegIndex r, const U &val) {
		modify(l, r, val, root, 0, n - 1);
	}

	T query(SegIndex l, SegIndex r) {
		return query(l, r, root, 0, n - 1);
	}

};
// }}}

// vim: fdm=marker
