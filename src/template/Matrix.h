#include <vector>
#include <iostream>
using namespace std;

// Matrix Template {{{
template <class T>
class Mat : public vector<vector<T>> {
private:
	int n = (*this).size();
	int m = (*this)[0].size();

public:
	template <class ...U>
	Mat(U &&...args) : vector<vector<T>>(forward<U>(args)...) {}
	explicit Mat(int n, int m) : Mat(n, vector<T>(m)) {}
	explicit Mat(int n) : Mat(n, n) {}

	static Mat<T> I(int n) {
		Mat res(n);
		for (int i=0;i<n;i++) res[i][i] = 1;
		return res;
	}

	Mat operator*(const Mat &rhs) const {
		Mat res(n, rhs.m);
		for (int i=0;i<n;i++) for (int j=0;j<res.m;j++) for (int k=0;k<m;k++) res[i][j] += (*this)[i][k] * rhs[k][j];
		return res;
	}

	Mat operator+(const Mat &rhs) const {
		Mat res(n, m);
		for (int i=0;i<n;i++) for (int j=0;j<m;j++) res[i][j] = (*this)[i][j] + rhs[i][j];
		return res;
	}

	Mat operator-(const Mat &rhs) const {
		Mat res(n, m);
		for (int i=0;i<n;i++) for (int j=0;j<m;j++) res[i][j] = (*this)[i][j] - rhs[i][j];
		return res;
	}

	Mat operator-() const {
		Mat res(n, m);
		for (int i=0;i<n;i++) for (int j=0;j<m;j++) res[i][j] = -(*this)[i][j];
		return res;
	}

	Mat operator^(long long x) const {
		Mat bas(*this), res(n);
		for (int i=0;i<n;i++) res[i][i] = 1;
		for (;x;x>>=1,bas=bas*bas) if (x & 1) res = res * bas;
		return res;
	}

	Mat submatrix(int x, int y, int xx, int yy) const {
		Mat res(xx - x + 1, yy - y + 1);
		for (int i=x;i<=xx;i++) for (int j=y;j<=yy;j++) res[i-x][j-y] = (*this)[i][j];
		return res;
	}

	Mat concath(const Mat &rhs) const {
		Mat res(n, m + rhs.m);
		for (int i=0;i<n;i++) {
			for (int j=0;j<m;j++) res[i][j] = (*this)[i][j];
			for (int j=0;j<rhs.m;j++) res[i][m+j] = rhs[i][j];
		}
		return res;
	}

	Mat &gauselim() {
		Mat &a = *this;
		// row r as pivot
		for (int r=0;r<n;r++) {
			// find mxrow
			T mx = 0; int mxr = -1;
			for (int i=r;i<n;i++) if (mx != a[i][r]) mx = a[i][r], mxr = i;  // change to < for floating point for stability
			if (mxr == -1) throw runtime_error("matrix not invertible");
			for (int j=0;j<m;j++) swap(a[r][j], a[mxr][j]);

			// make first term 1
			T ft = a[r][r];
			for (int j=r;j<m;j++) a[r][j] = a[r][j] / ft;

			// make a[i][r] = 0 for all i > r
			for (int i=r+1;i<n;i++) {
				T needsub = a[i][r];
				for (int j=r;j<m;j++) a[i][j] -= needsub * a[r][j];
			}
		}
		// backward sub
		for (int r=n-1;r>=0;r--) {
			for (int j=r+1;j<n;j++) {
				T needsub = a[r][j];
				for (int k=0;k<m;k++) a[r][k] -= needsub * a[j][k];
			}
		}
		return a;
	}

	Mat inverse() const {
		Mat gus = concath(Mat::I(n));
		gus.gauselim();
		return gus.submatrix(0, m, n-1, m+m-1);
	}

	friend ostream &operator<<(ostream &os, const Mat<T> &mt) {
		for (int i=0;i<mt.n;i++) for (int j=0;j<mt.m;j++) os << mt[i][j] << " \n"[j==mt.m-1];
		return os;
	}

	friend istream &operator>>(istream &is, Mat<T> &mt) {
		for (int i=0;i<mt.n;i++) for (int j=0;j<mt.m;j++) is >> mt[i][j];
		return is;
	}

};
// }}}

// vim: fdm=marker
