#include <functional>

// Treap {{{
template<class T, class Compare = std::less<T>>
class Treap {
	struct Node {
		T key;
		int prior;
		size_t sz;
		Node *l, *r;
		Node(const T &key, int prior) : key(key), prior(prior), sz(0), l(nullptr), r(nullptr) {}
		Node(const T &key) : Node(key, rng()) {}
		~Node() { delete l; delete r; }
	};
	Compare compare;
	Node *root;

	void split(Node *t, const T &key, Node *&l, Node *&r) {
		if (t == nullptr)
			l = r = NULL;
		else if (compare(key, t->key))
			split(t->l, key, l, t->l), r = t;
		else
			split(t->r, key, t->r, r), l = t;
		push_up(t);
	}

	void insert(Node *&t, Node *it) {
		if (t == nullptr)
			t = it;
		else if (it->prior > t->prior)
			split(t, it->key, it->l, it->r), t = it;
		else if (compare(it->key, t->key))
			insert(t->l, it);
		else
			insert(t->r, it);
		push_up(t);
	}

	void merge(Node *&t, Node *l, Node *r) {
		if (l == nullptr)
			t = r;
		else if (r == nullptr)
			t = l;
		else if (l->prior > r->prior)
			merge(l->r, l->r, r), t = l;
		else
			merge(r->l, l, r->l), t = r;
		push_up(t);
	}

	void erase(Node *&t, const T &key) {
		if (compare(key, t->key)) {
			erase(t->l, key);
		} else if (compare(t->key, key)) {
			erase(t->r, key);
		} else {
			Node *th = t;
			merge(t, t->l, t->r);
			th->l = th->r = nullptr;
			delete th;
		}
		push_up(t);
	}

	Node *unite(Node *l, Node *r) {
		if (l == nullptr) return r;
		if (r == nullptr) return l;
		if (l->prior < r->prior) swap(l, r);
		Node *lt, rt;
		split(r, l->key, lt, rt);
		l->l = unite(l->l, lt);
		l->r = unite(l->r, rt);
		return l;
	}

	size_t size(Node *t) {
		if (t == nullptr) return 0;
		return t->sz;
	}

	void push_up(Node *t) {
		if (t == nullptr) return;
		t->sz = size(t->l) + size(t->r) + 1;
	}
	
	T *find_by_order(Node *t, size_t order) {
		if (t == nullptr) return nullptr;
		size_t l_size = size(t->l);
		if (order == l_size) return &t->key;
		if (order < l_size) return find_by_order(t->l, order);
		return find_by_order(t->r, order - l_size - 1);
	}

	size_t order_of_key(Node *t, const T &key) {
		if (t == nullptr) return 0;
		if (compare(t->key, key)) {
			return size(t->l) + order_of_key(t->r, key) + 1;
		} else {
			return order_of_key(t->l, key);
		}
	}

	void heapify(Node *t) {
		if (t == nullptr) return;
		Node *max = t;
		if (t->l != nullptr && t->l->prior > max->prior)
        	max = t->l;
		if (t->r != nullptr && t->r->prior > max->prior)
			max = t->r;
		if (max != t) {
			swap(t->prior, max->prior);
			heapify(max);
		}
	}

	Node *build(const vector<T> &a, size_t l, size_t r) {
		if (l >= r) return nullptr;
		int mid = l + (r - l) / 2;
		Node *t = new Node(a[mid]);
		t->l = build(a, l, mid);
		t->r = build(a, mid + 1, r);
		heapify(t);
		push_up(t);
		return t;
	}

public:
	Treap() : root(nullptr) {}
	Treap(const vector<T> &keys) {
		root = build(keys, 0, keys.size());
	}
	~Treap() { delete root; }
	size_t size() {
		return size(root);
	}
	bool empty() {
		return size() == 0;
	}
	void insert(const T &value) {
		insert(root, new Node(value));
	}
	void erase(const T &key) {
		erase(root, key);
	}
	T *find_by_order(size_t order) {
		return find_by_order(root, order);
	}
	size_t order_of_key(const T &key) {
		return order_of_key(root, key);
	}
};
// }}}

// vim: fdm=marker
