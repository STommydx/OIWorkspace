#include <deque>
#include <functional>
using namespace std;

// Monotone Queue {{{
template<class T, class Compare = less<T>>
class MonoQueue {
private:
	deque<pair<T, size_t>> dq;
	size_t cnt_added = 0;
	size_t cnt_removed = 0;
	Compare comp;
public:
	void push(const T &x) {
		while (!dq.empty() && comp(dq.back().first, x)) {
			dq.pop_back();
		}
		dq.emplace_back(x, cnt_added);
		cnt_added++;
	}
	void pop() {
		if (!dq.empty() && dq.front().second == cnt_removed) 
			dq.pop_front();
		cnt_removed++;
	}
	T top() {
		return dq.front().first;
	}
	size_t size() {
		return cnt_added - cnt_removed;
	}
	bool empty() {
		return cnt_added == cnt_removed;
	}
};
// }}}