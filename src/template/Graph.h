#include <vector>
#include <iostream>
#include "operators.h"
using namespace std;

// Graph Template {{{
template <class T = void, bool DIRECTED = false>
class Graph : public vector<vector<pair<int, T>>> {
	private:
		int n, m;
	public:
		Graph(int n, int m = 0) : vector<vector<pair<int, T>>>(n), n(n), m(m) {}

		friend istream &operator>>(istream &is, Graph &g);

		template<class ...Args>
		void addDiEdge(int u, int v, Args &&...args) { (*this)[u].emplace_back(v, std::forward<Args>(args)...); }

		template<class ...Args>
		void addEdge(int u, int v, Args &&...args) {
			addDiEdge(u, v, std::forward<Args>(args)...);
			if (!DIRECTED) addDiEdge(v, u, std::forward<Args>(args)...);
		}

		pair<vector<T>, vector<int>> dijkstra(vector<int> src) const {
			const T &INF = Inf<T>::inf();
			auto &g = *this;
			vector<T> dist(n, INF);
			vector<int> par(n, -1);
			min_queue<pair<T, int>> q;
			for (int s : src) q.emplace(dist[s] = 0, s);
			while (!q.empty()) {
				auto [du, u] = q.top(); q.pop();
				if (du > dist[u]) continue;
				for (auto [v, l] : g[u]) if (T dv = dist[u] + l; dv < dist[v])
					q.emplace(dist[v] = dv, v), par[v] = u;
			}
			return {dist, par};
		}

		tuple<vector<T>, vector<int>, vector<int>> prims(int src) const {
			const T &INF = Inf<T>::inf();
			auto &g = *this;
			vector<T> dist(n, INF);
			vector<int> par(n, -1), dep(n), vis(n);
			min_queue<pair<T, int>> q;
			q.emplace(dist[src] = 0, src);
			while (!q.empty()) {
				auto [du, u] = q.top(); q.pop();
				vis[u] = true;
				for (auto [v, l] : g[u]) if (T dv = l; !vis[v] && dv < dist[v])
					q.emplace(dist[v] = dv, v), par[v] = u, dep[v] = dep[u] + 1;
			}
			return {dist, par, dep};
		}

		vector<int> biColoring() const {
			auto &g = *this;
			bool wtf = false;
			vector<int> res(n, -1);
			function<void(int)> dfs = [&] (int u) {
				for (auto [v, l] : g[u]) {
					if (res[v] == -1) {
						res[v] = 1 - res[u];
						dfs(v);
					} else if (res[v] == res[u]) {
						wtf = true;
						return;
					}
				}
			};
			for (int i = 0; i < n && !wtf; i++) if (res[i] == -1) {
				res[i] = 0;
				dfs(i);
			}
			if (wtf) return vector<int>();
			return res;
		}

		pair<vector<int>, vector<int>> dfsTime() const {
			auto &g = *this;
			const int INF = Inf<int>::inf();
			vi stTime(n, INF), fiTime(n, INF);
			int timer = 0;
			function<void(int)> dfs = [&] (int u) {
				stTime[u] = timer++;
				for (auto [v, l] : g[u]) if (stTime[v] >= INF) dfs(v);
				fiTime[u] = timer;
			};
			for (int i = 0; i < n; i++) if (stTime[i] >= INF) dfs(i);
			return {stTime, fiTime};
		}

		vector<int> preOrder() const {
			auto [stTime, fiTime] = dfsTime();
			vi ord(n);
			for (int i = 0; i < n; i++) ord[stTime[i]] = i;
			return ord;
		}

		vector<int> postOrder() const {
			auto &g = *this;
			vi vis(n);
			vi ord;
			function<void(int)> dfs = [&] (int u) {
				vis[u] = true;
				for (auto [v, l] : g[u]) if (!vis[v]) dfs(v);
				ord.push_back(u);
			};
			for (int i = 0; i < n; i++) if (!vis[i]) dfs(i);
			return ord;
		}

		vector<int> topSort() const {
			vi pord = postOrder();
			reverse(pord.begin(), pord.end());
			return pord;
		}

		vector<int> sccColoring() const {
			auto &g = *this;
			vector<vector<int>> gr(n);
			for (int u = 0; u < n; u++) for (auto [v, l] : g[u]) gr[v].push_back(u);
			vector<int> c(n, -1);
			int ccnt = 0;
			function<void(int)> dfs = [&] (int u) {
				for (int v : gr[u]) if (c[v] == -1) {
					c[v] = c[u];
					dfs(v);
				}
			};
			for (int u : topSort()) if (c[u] == -1) c[u] = ccnt++, dfs(u);
			return c;
		}

};
template <class T> using DiGraph = Graph<T, true>;
template <class U, bool D>
istream &operator>>(istream &is, Graph<U, D> &g) {
	for (int i=0;i<g.m;i++) {
		int u, v; U l; is >> u >> v >> l; u--, v--;
		g.addEdge(u, v, l);
	}
	return is;
}
template <class U, bool D>
istream &operator>>(istream &is, Graph<void, D> &g) {
	for (int i=0;i<g.m;i++) {
		int u, v; is >> u >> v; u--, v--;
		g.addEdge(u, v);
	}
	return is;
}
// }}}

// Fake Pair Template {{{
namespace std {
	template <class T>
	class pair<T, void> {
		public:
			typedef T first_type;
			T first;

			constexpr pair(const T &x) : first(x) {}
			constexpr pair(const T &x, const T &) : pair(x) {}
			constexpr pair() : first() {}
			constexpr pair(const pair &p) = default;
			constexpr pair(pair &&p) = default;

			template <class U1>
			constexpr pair(U1 &&x) : first(std::forward<U1>(x)) {}
			template <class U1, class U2>
			constexpr pair(U1 &&x, U2 &&y) : pair(std::forward<U1>(x)) {}
			template <class U1, class U2>
			constexpr pair(const pair<U1, U2> &p) : first(p.first) {}
			template <class U1, class U2>
			constexpr pair(pair<U1, U2> &&p) : first(std::forward<U1>(p.first)) {}

			constexpr pair &operator=(const pair &other) {
				first = other.first;
				return *this;
			}
			constexpr pair &operator=(const pair &&other) {
				first = std::forward<T>(other.first);
				return *this;
			}
			template <class U1, class U2>
			constexpr pair &operator=(const pair<U1, U2> &other) {
				first = other.first;
				return *this;
			}
			template <class U1, class U2>
			constexpr pair &operator=(pair<U1, U2> &&other) {
				first = std::forward<U1>(other.first);
				return *this;
			}

			void swap(pair &p) { std::swap(first, p.first); }

			operator T() const { return first; }

	};

	template <class U1>
	void swap(pair<U1, void> &lhs, pair<U1, void> &rhs) { lhs.swap(rhs); }

	template <class T>
	class tuple_element<1, pair<T, void>> { public: typedef T type; };

	template <size_t I, class T>
	T &get(pair<T, void> &p) { return p.first; }
	template <size_t I, class T>
	T &&get(pair<T, void> &&p) { return std::forward<T>(p.first); }
	template <size_t I, class T>
	const T &get(const pair<T, void> &p) { return p.first; }
	template <size_t I, class T>
	const T &&get(const pair<T, void> &&p) { return std::forward<T>(p.first); }
}

// }}}

// 2-SAT {{{
class TSAT {
	private:
		int n;
		DiGraph<void> g;

	public:
		TSAT(int n) : n(n), g(n + n) {}
		void addTiT(int u, int v) { g.addEdge(u, v); }
		void addTiF(int u, int v) { g.addEdge(u, n + v); }
		void addFiT(int u, int v) { g.addEdge(n + u, v); }
		void addFiF(int u, int v) { g.addEdge(n + u, n + v); }
		vector<int> assign() {
			vector<int> clr = g.sccColoring();
			vector<int> asg(n);
			for (int i = 0; i < n; i++) {
				if (clr[i] == clr[n + i]) return vector<int>();
				asg[i] = clr[i] > clr[n + i];
			}
			return asg;
		}
};
// }}}

// LCA {{{
template <class T = void, class Combinator = Combine::Max<T>, int M = 20>
class LCA {
		static Combinator combinator;
		int n;
		vector<int> dep;
		vector<vector<T>> dp2;
		vector<vector<int>> dp;
	public:
		LCA(const vector<int> &par, const vector<int> &dep, const vector<T> &v = vector<T>()) : n(par.size()), dep(dep), dp2(M, vector<T>(n, combinator.def())), dp(M, vector<int>(n, -1)) {
			dp2[0] = v;
			dp[0] = par;
			for (int j = 1; j < M; j++) for (int i = 0; i < n; i++) {
				if (dp[j - 1][i] == -1) continue;
				dp2[j][i] = combinator(dp2[j - 1][i], dp2[j - 1][dp[j - 1][i]]);
				dp[j][i] = dp[j - 1][dp[j - 1][i]];
			}
		}
		pair<T, int> operator()(int u, int v) const {
			T ans = combinator.def();
			if (dep[u] > dep[v]) swap(u, v);
			for (int j = M - 1; j >= 0; j--) if (int nv = dp[j][v]; nv != -1 && dep[nv] >= dep[u])
				ans = combinator(ans, dp2[j][v]), v = nv;
			if (u == v) return {ans, v};
			for (int j = M - 1; j >= 0; j--) if (dp[j][u] != dp[j][v])
				ans = combinator(ans, dp2[j][u]), ans = combinator(ans, dp2[j][v]), u = dp[j][u], v = dp[j][v];
			return {combinator(ans, combinator(dp2[0][u], dp2[0][v])), dp[0][u]};
		}
};
template<class Combinator, int M>
class LCA<void, Combinator, M> {
		int n;
		vector<int> dep;
		vector<vector<int>> dp;
	public:
		LCA(const vector<int> &par, const vector<int> &dep) : n(par.size()), dep(dep), dp(M, vector<int>(n, -1)) {
			dp[0] = par;
			for (int j = 1; j < M; j++) for (int i = 0; i < n; i++) {
				if (dp[j - 1][i] == -1) continue;
				dp[j][i] = dp[j - 1][dp[j - 1][i]];
			}
		}
		int operator()(int u, int v) const {
			if (dep[u] > dep[v]) swap(u, v);
			for (int j = M - 1; j >= 0; j--) if (int nv = dp[j][v]; nv != -1 && dep[nv] >= dep[u])
				v = nv;
			if (u == v) return v;
			for (int j = M - 1; j >= 0; j--) if (dp[j][u] != dp[j][v])
				u = dp[j][u], v = dp[j][v];
			return dp[0][u];
		}
};
// }}}

// vim: fdm=marker
