#include <string>
using namespace std;

// GCD Template {{{
template <class T> T gcd(T a, T b) { return b ? gcd(b, a % b) : a; }
template <class T> T lcm(T a, T b) { return a / gcd(a, b) * b; }
// }}}

// Binary Exponentiation Template {{{
template <class T, class U>
inline U poo(T b, T p, U md) {
	T rt = 1;
	for(; p; p >>= 1, b = b * b % md) if (p & 1) rt = rt * b % md;
	return rt;
}
// }}}

// Fraction Template {{{
template <class T>
class Fraction : public pair<T, T> {
	private:
		void normalize() {
			T cf = gcd(abs(this->first), abs(this->second));
			this->first /= cf; this->second /= cf;
			if (this->second < 0) this->first *= -1, this->second *= -1;
		}
	public:
		Fraction(const pair<T, T> &pr) : pair<T, T>{pr} { normalize(); }
		Fraction(const T &p, const T &q) : Fraction{pair<T, T>{p, q}} {}
		Fraction(const T &p) : Fraction{p, T(1)} {}
		Fraction &operator+=(const Fraction<T> &rhs) {
			this->first *= rhs.second;
			this->first += this->second * rhs.first;
			this->second *= rhs.second;
			normalize();
			return *this;
		}
		Fraction operator+(const Fraction<T> &rhs) const { return Fraction<T>{*this} += rhs; }
		Fraction &operator*=(const Fraction<T> &rhs) {
			this->first *= rhs.first;
			this->second *= rhs.second;
			normalize();
			return *this;
		}
		Fraction operator*(const Fraction<T> &rhs) const { return Fraction<T>{*this} *= rhs; }
		Fraction operator-() const { return Fraction<T>{-this->first, this->second}; }
		Fraction &operator-=(const Fraction<T> &rhs) { return operator+=(-rhs); }
		Fraction operator-(const Fraction<T> &rhs) const { return Fraction<T>{*this} -= rhs; }
		Fraction inv() const { return Fraction<T>{this->second, this->first}; }
		Fraction &operator/=(const Fraction<T> &rhs) { return operator*=(rhs.inv()); }
		Fraction operator/(const Fraction<T> &rhs) { return Fraction{*this} /= rhs; }
		bool operator<(const Fraction<T> &rhs) const { return this->first * rhs.second < rhs.first * this->second; }
		// bool operator<(const Fraction<T> &rhs) const { ll wtf = lcm(this->second, rhs.second); return wtf / this->second * this->first < wtf / rhs.second * rhs.first; }
		bool operator>(const Fraction<T> &rhs) const { return rhs < *this; }
		bool operator<=(const Fraction<T> &rhs) const { return !operator>(rhs); }
		bool operator>=(const Fraction<T> &rhs) const { return !operator<(rhs); }
		bool operator!=(const Fraction<T> &rhs) const { return operator<(rhs) || operator>(rhs); }
		bool operator==(const Fraction<T> &rhs) const { return !operator!=(rhs); }
		static Fraction fromDecimalString(const std::string &str) {
			T q = 1;
			auto pos = str.find('.');
			if (pos == string::npos) {
				return stoll(str);
			} else {
				string fst = str.substr(0, pos);
				string sec = str.substr(pos + 1);
				for (size_t i=0;i<sec.size();i++) q *= 10;
				T p1 = stoll(fst);
				T p2 = stoll(sec);
				return {p1 * q + p2, q};
			}
		}
		template <class Q>
		friend Fraction<Q> abs(const Fraction<Q> &lhs) { return Fraction<Q>{abs(lhs.first), lhs.second}; }
};
// }}}

// Coordinate Compression Vector {{{
template <class T>
class CVector : public vector<T> {
	public:
		template <class... U> CVector(U &&... args) : vector<T>(forward<U>(args)...) {}

		void compress() {
			sort(this->begin(), this->end());
			this->erase(unique(this->begin(), this->end()), this->end());
		}

		auto getlb(const T &v) {
			return distance(this->begin(), lower_bound(this->begin(), this->end(), v));
		}

		auto getub(const T &v) {
			return distance(this->begin(), upper_bound(this->begin(), this->end(), v));
		}

		T getSegLen(int i) {
			T rt = this->at(i);
			if (i > 0) rt -= this->at(i - 1);
			return rt;
		}
};
// }}}

// Prefix Sum {{{
template <class T>
class PrefixSum : public vector<T> {
	public:
		template<class ...Args>
		PrefixSum(Args &&...args) : vector<T>(forward<Args>(args)...) {
			auto &a = *this; int n = a.size();
			for (int i = 1; i < n; i++) a[i] += a[i - 1];
		}
		T query(int l, int r) {
			auto &a = *this; T res = a[r];
			if (l > 0) res -= a[l - 1];
			return res;
		}
		T query(int x) {
			return (*this)[x];
		}
};
template <class T>
class TDPrefixSum : public vector<vector<T>> {
	public:
		template<class ...Args>
		TDPrefixSum(Args &&...args) : vector<vector<T>>(forward<Args>(args)...) {
			auto &a = *this;
			int n = a.size(), m = a[0].size();
			for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
				if (i > 0) a[i][j] += a[i - 1][j];
				if (j > 0) a[i][j] += a[i][j - 1];
				if (i > 0 && j > 0) a[i][j] -= a[i - 1][j - 1];
			}
		}
		T query(int lx, int ly, int rx, int ry) {
			auto &a = *this;
			T sm = a[rx][ry];
			if (lx > 0) sm -= a[lx - 1][ry];
			if (ly > 0) sm -= a[rx][ly - 1];
			if (lx > 0 && ly > 0) sm += a[lx - 1][ly - 1];
			return sm;
		}
};
// }}}

// Diff Array {{{
template <class T>
class DiffArray : public vector<T> {
public:
	template <class ...Args>
	DiffArray(Args &&...args) : vector<T>(forward<Args>(args)...) {}
 
	void modify(int l, int r, int v) {
		if (l > r) return;
		(*this)[l] += v;
		if (r + 1 < int(this->size())) (*this)[r + 1] -= v;
	}
 
	vector<T> build() {
		vector<T> res(this->size());
		partial_sum(this->begin(), this->end(), res.begin());
		return res;
	}
};
// }}}

// vim: fdm=marker
