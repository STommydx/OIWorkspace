#include <iostream>
#include <vector>
using namespace std;

// Automod Integer Template {{{
template <int MOD = 1'000'000'007>
class Mint {
private:
	int x;

public:
	template<int MOD_>
	friend inline ostream &operator<<(ostream &os, const Mint<MOD_> &arg) {
		return os << arg.x;
	}
	template<int MOD_>
	friend inline istream &operator>>(istream &is, Mint<MOD_> &arg) {
		is >> arg.x;
		if (arg.x >= MOD) arg.x -= MOD;
		return is;
	}
	constexpr Mint(const int &x) : x(x) {
		if (this->x >= MOD) this->x -= MOD;
		if (this->x < 0) this->x += MOD;
	}
	constexpr Mint(const long long &x) : x(x % MOD) {}
	constexpr Mint() : x(0) {}

	constexpr Mint &operator+=(const Mint &rhs) {
		x += rhs.x;
		if (x >= MOD) x -= MOD;
		return *this;
	}
	constexpr Mint &operator++() { return *this += 1; }
	constexpr Mint operator+(const Mint &rhs) const { return Mint(*this) += rhs; }
	constexpr Mint operator++(int) {
		Mint cpy(*this);
		++*this;
		return cpy;
	}

	constexpr Mint &operator-=(const Mint &rhs) {
		x -= rhs.x;
		if (x < 0) x += MOD;
		return *this;
	}
	constexpr Mint &operator--() { return *this -= 1; }
	constexpr Mint operator-(const Mint &rhs) const { return Mint(*this) -= rhs; }
	constexpr Mint operator-() const { return Mint() - *this; }
	constexpr Mint operator--(int) {
		Mint cpy(*this);
		--*this;
		return cpy;
	}

	constexpr Mint &operator*=(const Mint &rhs) {
		x = 1LL * x * rhs.x % MOD;
		return *this;
	}
	constexpr Mint operator*(const Mint &rhs) const { return Mint(*this) *= rhs; }

	constexpr Mint pow(long long p) const {
		Mint rt = 1, b = *this;
		for (;p;p>>=1,b*=b) if (p & 1) rt *= b;
		return rt;
	}

	constexpr Mint operator^(long long p) const { return pow(p); }
	constexpr Mint &operator^=(long long p) { return *this = pow(p); }

	constexpr Mint &operator/=(const Mint &rhs) { return *this *= rhs.pow(MOD - 2); }
	constexpr Mint operator/(const Mint &rhs) const { return Mint(*this) /= rhs; }

	constexpr bool operator==(const Mint &rhs) const { return x == rhs.x; }
	constexpr bool operator!=(const Mint &rhs) const { return x != rhs.x; }

	constexpr explicit operator int() const { return x; }
	constexpr explicit operator bool() const { return bool(x); }

};
template <int MOD> struct hash<Mint<MOD>> {
	size_t operator()(const Mint<MOD> &s) const noexcept {
		return hash<int>{}(int(s));
    }
};
using mint = Mint<>;
// using mint = Mint<998'244'353>;
using vm = vector<mint>;
using vvm = vector<vm>;
using pmm = pair<mint, mint>;
// }}}

// vim: fdm=marker
