// Template Headers {{{
#include <bits/stdc++.h>
using namespace std;

using vi = vector<int>; using vvi = vector<vi>; using pii = pair<int, int>; using vpii = vector<pii>;
using ll = long long; using vll = vector<long long>; using vvll = vector<vll>; using pll = pair<ll, ll>; using vpll = vector<pll>;
template <class T> using min_queue = priority_queue<T, vector<T>, greater<T>>;
template <class T> istream &operator>>(istream &, vector<T> &); template <class T> ostream &operator<<(ostream &, const vector<T> &);
template <class T, class U> istream &operator>>(istream &, pair<T, U> &); template <class T, class U> ostream &operator<<(ostream &, const pair<T, U> &);
template <class T = int> constexpr T INFTY = numeric_limits<T>::has_infinity ? numeric_limits<T>::infinity() : numeric_limits<T>::max();
template <> constexpr int INFTY<int> = 0x3f3f3f3f; template <> constexpr ll INFTY<ll> = 0x3f3f3f3f3f3f3f3fLL;
template <class T, class U> constexpr pair<T, U> INFTY<pair<T, U>> = make_pair(INFTY<T>, INFTY<U>);
constexpr int INF = INFTY<>; constexpr ll BINF = INFTY<ll>;
// mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());
// }}}
// Preprocessing & "Global" variables {{{
struct Solver {
	Solver() {}
	int solve();
};
// }}}

#define JUDGE_MULTI_CASE

int Solver::solve() {

	return 0;
}

// Template Main {{{
int main() {
	ios::sync_with_stdio(false); cin.tie(nullptr);
	Solver solver;
	#ifdef JUDGE_MULTI_CASE
		int t; cin >> t; for (int i = 1; i <= t; i++)
	#endif
	solver.solve();
	// cout << "Case #" << i << ": ", solver.solve();
	return 0;
}

template <class T> istream &operator>>(istream &is, vector<T> &v) {
	for (auto it=v.begin();it!=v.end();++it) is >> *it;
	return is;
}

template <class T> ostream &operator<<(ostream &os, const vector<T> &v) {
	for (auto it=v.begin();it!=v.end();) os << *it, os << " \n"[++it==v.end()];
	return os;
}

template <class T, class U> istream &operator>>(istream &is, pair<T, U> &p) {
	return is >> p.first >> p.second;
}

template <class T, class U> ostream &operator<<(ostream &os, const pair<T, U> &p) {
	return os << p.first << ' ' << p.second;
}
// }}}

// vim: fdm=marker
