#include <string>
#include <algorithm>

// Custom Hash Utility {{{
class MyHash {
	private:
		static int MODS[], BAS;
		static const int MX = 2;
		int len = 0;
		int dat[MX]{};
		inline int poo(long long b, int p, int md) {
			long long rt = 1;
			for(; p; p >>= 1, b = b * b % md) if (p & 1) rt = rt * b % md;
			return rt;
		}
	public:
		MyHash() {}
		MyHash(const std::string &s) {
			for(char c: s) push_back(c);
		}

		void push_back(int c) {
			for(int i = 0; i < MX; i++){
				dat[i] = (1LL * dat[i] * BAS + c) % MODS[i];
			}
			len++;
		}

		void push_front(int c) {
			for (int i = 0; i < MX; i++) {
				dat[i] = (dat[i] + poo(BAS, len, MODS[i])) % MODS[i];
			}
			len++;
		}

		void pop_front(int c) {
			for(int i = 0; i < MX; i++){
				dat[i] -= 1LL * c * poo(BAS, len - 1, MODS[i]) % MODS[i];
				dat[i] = (dat[i] + MODS[i]) % MODS[i];
			}
			len--;
		}

		bool operator<(const MyHash &rhs) {
			return std::lexicographical_compare(dat, dat+MX, rhs.dat, rhs.dat+MX);
		}

		bool operator==(const MyHash &rhs) {
			return std::equal(dat, dat+MX, rhs.dat);
		}
};
int MyHash::MODS[] = {1000000007, 1000000009};
int MyHash::BAS = 43;
// }}}

// vim: fdm=marker
