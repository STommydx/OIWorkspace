#include <algorithm>
using namespace std;

// Matrix Template {{{
namespace np {

	template <class T, int N = 1>
	class ndarray;

	template <class T> auto stack(const vector<T> &vs, int axis = 0);
	template <class T, int N> auto stack(const vector<ndarray<T, N>> &vs, int axis = 0);

	template <class T, int N>
	class ndarray : public vector<ndarray<T, N - 1>> {
	public:
		explicit ndarray() = default;
		template <class ...U>
		explicit ndarray(int n, U &&...args) : vector<ndarray<T, N - 1>>(n, ndarray<T, N - 1>(forward<U>(args)...)) {}
		template <class U>
		explicit ndarray(const vector<U> &v) {
			this->reserve(v.size());
			for (auto vi : v) this->emplace_back(vi);
		}
		auto shape() const {
			return tuple_cat(make_tuple(this->size()), (*this)[0].shape());
		}
		ndarray<T, 1> flatten() const {
			vector<ndarray<T, 1>> res; res.reserve(this->size());
			for (auto &vi : *this) res.push_back(vi.flatten());
			return concat(res);
		}

		template <class ...Args> 
		ndarray<T, sizeof...(Args)> reshape(Args &&...args) {
			return flatten().reshape(forward<Args>(args)...);
		}

		template <class ...Args>
		auto slice(typename ndarray::size_type idx, Args &&...args) {
			return (*this)[idx].slice(forward<Args>(args)...);
		}

		template <class ...Args>
		auto slice(initializer_list<typename ndarray::size_type> indices, Args &&...args) {
			using U = decay_t<decltype((*this)[0].slice(forward<Args>(args)...))>;
			auto slicer = [&args...] (auto &vi) { return vi.slice(forward<Args>(args)...); };
			vector<U> v;
			if (indices.size() == 0) {
				// take all
				transform(this->begin(), this->end(), back_inserter(v), slicer);
			} else if (indices.size() == 1) {
				// take range
				auto it = indices.begin();
				auto from = *(it++); if (from < 0) from += this->size();
				transform(this->begin() + from, this->end(), back_inserter(v), slicer);
			} else if (indices.size() == 2) {
				// take range
				auto it = indices.begin();
				auto from = *(it++); if (from < 0) from += this->size();
				auto to = *(it++); if (to < 0) to += this->size();
				transform(this->begin() + from, this->begin() + to, back_inserter(v), slicer);
			} else if (indices.size() == 3) {
				// take range with step
				auto it = indices.begin();
				auto from = *(it++); if (from < 0) from += this->size();
				auto to = *(it++); if (to < 0) to += this->size();
				auto step = *(it++);
				if (step > 0) {
					for (auto i = from; i < to; i += step) {
						v.push_back(slicer((*this)[i]));
					}
				} else {
					for (auto i = from; i > to; i += step) {
						v.push_back(slicer((*this)[i]));
					}
				}
			}
			return stack(v);
		}

		auto slice() {
			return *this;
		}

	};

	template <class T>
	class ndarray<T, 1> : public vector<T> {
	public:
		explicit ndarray() = default;
		template <class ...U>
		explicit ndarray(U &&...args) : vector<T>(forward<U>(args)...) {}
		auto shape() const {
			return make_tuple(this->size());
		}
		ndarray flatten() const {
			return *this;
		}

		ndarray reshape(typename ndarray::size_type n) const {
			if (n != this->size()) throw runtime_error("invalid shape: " + to_string(this->size()) + " != " + to_string(n));
			return *this;
		}
		
		template <class ...Args> 
		ndarray<T, sizeof...(Args) + 1> reshape(typename ndarray::size_type n, Args &&...args) const {
			if (this->size() % n) throw runtime_error("invalid shape: " + to_string(this->size()) + " % " + to_string(n) + " != 0");
			ndarray<T, sizeof...(Args) + 1> rt;
			for (typename ndarray::size_type i = 0; i < this->size(); i += this->size() / n) {
				ndarray tmp(this->begin() + i, this->begin() + i + (this->size() / n));
				rt.push_back(tmp.reshape(forward<Args>(args)...));
			}
			return rt;
		}

		auto slice() {
			return *this;
		}

		auto slice(typename ndarray::size_type idx) {
			return (*this)[idx];
		}

		auto slice(initializer_list<typename ndarray::size_type> indices) {
			vector<T> v;
			if (indices.size() == 0) {
				// take all
				return *this;
			} else if (indices.size() == 1) {
				// take range
				auto it = indices.begin();
				auto from = *(it++); if (from < 0) from += this->size();
				copy(this->begin() + from, this->end(), back_inserter(v));
			} else if (indices.size() == 2) {
				// take range
				auto it = indices.begin();
				auto from = *(it++); if (from < this->size()) from += this->size();
				auto to = *(it++); if (to < 0) to += this->size();
				copy(this->begin() + from, this->begin() + to, back_inserter(v));
			} else if (indices.size() == 3) {
				// take range with step
				auto it = indices.begin();
				auto from = *(it++); if (from < 0) from += this->size();
				auto to = *(it++); if (to < 0) to += this->size();
				auto step = *(it++);
				if (step > 0) {
					for (auto i = from; i < to; i += step) {
						v.push_back((*this)[i]);
					}
				} else {
					for (auto i = from; i > to; i += step) {
						v.push_back((*this)[i]);
					}
				}
			}
			return ndarray<T, 1>(v);
		}
	};

	template <class T> auto concat(const vector<ndarray<T, 1>> &vs, int axis = 0) {
		ndarray<T, 1> res;
		for (auto &v : vs) copy(v.begin(), v.end(), back_inserter(res));
		return res;
	} 
	
	template <class T, int N> auto concat(const vector<ndarray<T, N>> &vs, int axis = 0) {
		if (axis == 0) {
			ndarray<T, N> res;
			for (auto &v : vs) copy(v.begin(), v.end(), back_inserter(res));
			return res;
		} else if (axis < 0) {
			return concat(vs, axis + N);
		} else {
			ndarray<T, N> res;
			for (typename ndarray<T, N>::size_type i = 0; i < vs.front().size(); i++) {
				vector<decay_t<decltype(vs.front()[i])>> vt; vt.reserve(vs.size());
				for (auto &vi : vs) vt.push_back(vi[i]);
				res.push_back(concat(vt, axis - 1));
			}
			return res;
		}
	} 

	template <class T> auto stack(const vector<T> &vs, int axis) {
		return ndarray<T>(vs);
	}

	template <class T, int N> auto stack(const vector<ndarray<T, N>> &vs, int axis) {
		if (axis == 0) {
			return ndarray<T, N + 1>(vs);
		} else if (axis < 0) {
			return stack(vs, axis + N);
		} else {
			ndarray<T, N + 1> res;
			for (typename ndarray<T, N>::size_type i = 0; i < vs.front().size(); i++) {
				vector<decay_t<decltype(vs.front()[i])>> vt; vt.reserve(vs.size());
				for (auto &vi : vs) vt.push_back(vi[i]);
				res.push_back(stack(vt, axis - 1));
			}
			return res;
		}
	}

    template <class T> auto expand_dims(const T &v, int axis) {
		ndarray<T, 1> r;
		r.push_back(v);
		return r;
    }

    template <class T, int N> auto expand_dims(const ndarray<T, N> &v, int axis) {
        if (axis == 0) {
            ndarray<T, N + 1> r;
            r.push_back(v);
            return r;
        } else if (axis < 0) {
            return expand_dims(v, axis + N);
        } else {
            ndarray<T, N + 1> r; r.reserve(v.size());
            for (auto &vi : v) r.push_back(expand_dims(vi, axis - 1));
            return r;
        }
    }

	template <class>
	class ufunc;

	template <class R, class T>
	class ufunc<R(T)> : function<R(T)> {
	public:
		template <class F>
		explicit ufunc(F &&f) : function<R(T)>(forward<F>(f)) {}

		R operator()(T t) const {
			return function<R(T)>::operator()(forward<T>(t));
		}

		template <int N>
		ndarray<decay_t<R>, N> operator()(const ndarray<decay_t<T>, N> &t) const {
			ndarray<decay_t<R>, N> r; r.reserve(t.size());
			transform(t.begin(), t.end(), back_inserter(r), *this);
			return r;
		}	
	};

	template <class R, class T1, class T2>
	class ufunc<R(T1, T2)> : function<R(T1, T2)> {
	private:
		R identity;
	public:
		template <class F>
		ufunc(const R &identity, F &&f) : function<R(T1, T2)>(forward<F>(f)), identity(identity) {}
		template <class F>
		explicit ufunc(F &&f) : function<R(T1, T2)>(forward<F>(f)) {}

		R operator()(T1 t1, T2 t2) const {
			return function<R(T1, T2)>::operator()(forward<T1>(t1), forward<T2>(t2));
		}
		
		template <int N1>
		auto operator()(const ndarray<decay_t<T1>, N1> &t1, T2 t2) const {
			return operator()(t1, expand_dims(t2, 0));
		}

		template <int N2>
		auto operator()(T1 t1, const ndarray<decay_t<T1>, N2> &t2) const {
			return operator()(expand_dims(t1, 0), t2);
		}

		template <int N1, int N2>
		auto operator()(const ndarray<decay_t<T1>, N1> &t1, const ndarray<decay_t<T2>, N2> &t2) const {
			if constexpr (N1 == N2) {
				ndarray<decay_t<R>, N1> r; r.reserve(t1.size());
				if (t1.size() == t2.size()) {
					transform(t1.begin(), t1.end(), t2.begin(), back_inserter(r), *this);
				} else if (t1.size() == 1) {
					transform(t2.begin(), t2.end(), back_inserter(r), [this, &t1] (const auto &t2i) { return this->operator()(t1.front(), t2i); });
				} else if (t2.size() == 1) {
					transform(t1.begin(), t1.end(), back_inserter(r), [this, &t2] (const auto &t1i) { return this->operator()(t1i, t2.front()); });
				} else {
					throw runtime_error("matrix dimension not equal: " + to_string(t1.size()) + " != " + to_string(t2.size()));
				}
				return r;
			} else if constexpr (N1 > N2) {
				return operator()(t1, expand_dims(t2, 0));
			} else {
				return operator()(expand_dims(t1, 0), t2);
			}
		}

		enable_if_t<is_convertible_v<decay_t<R>, decay_t<T1>>, R> reduce(const ndarray<decay_t<T2>, 1> &t2, int axis = 0) {
			return std::reduce(t2.begin(), t2.end(), identity, *this);
		}

		template <int N>
		enable_if_t<is_convertible_v<decay_t<R>, decay_t<T1>>, R> reduce(const ndarray<decay_t<T2>, N> &t2) {
			return reduce(reduce(t2, 0));
		}

		template <int N>
		enable_if_t<is_convertible_v<decay_t<R>, decay_t<T1>>, ndarray<R, N - 1>> reduce(const ndarray<decay_t<T2>, N> &t2, int axis) {
			if (axis == 0) {
				return std::reduce(t2.begin(), t2.end(), full_like(t2.front(), identity), *this);
			} else if (axis < 0) {
				return reduce(t2, axis + N);
			} else {
				ndarray<decay_t<R>, N - 1> r; r.reserve(t2.size());
				transform(t2.begin(), t2.end(), back_inserter(r), [this, axis] (const auto &t2i) { return reduce(t2i, axis - 1); });
				return r;
			}
		}

		enable_if_t<is_convertible_v<decay_t<R>, decay_t<T1>>, ndarray<R, 1>> accumulate(const ndarray<decay_t<T2>, 1> &t2, int axis = 0) {
			ndarray<decay_t<R>, 1> r; r.reserve(t2.size());
			partial_sum(t2.begin(), t2.end(), back_inserter(r), *this);
			return r;
		}

		template <int N>
		enable_if_t<is_convertible_v<decay_t<R>, decay_t<T1>>, ndarray<R, N>> accumulate(const ndarray<decay_t<T2>, N> &t2, int axis = 0) {
			if (axis == 0) {
				ndarray<decay_t<R>, N> r; r.reserve(t2.size());
				partial_sum(t2.begin(), t2.end(), back_inserter(r), *this);
				return r;
			} else if (axis < 0) {
				return accumulate(t2, axis + N);
			} else {
				ndarray<decay_t<R>, N> r; r.reserve(t2.size());
				transform(t2.begin(), t2.end(), back_inserter(r), [this, axis] (const auto &t2i) { return accumulate(t2i, axis - 1); });
				return r;
			}
		}
	};

	template <class T>
	ndarray<T, 2> matmul(const ndarray<T, 2> &l, const ndarray<T, 2> &r) {
		auto [n, m] = l.shape();
		auto [mr, k] = r.shape();
		if (m != mr) throw runtime_error("matrix with shape (" + to_string(n) + ", " + to_string(m) + ") cannot not mutiple matrix with shape (" + to_string(mr) + ", " + to_string(k) + ")"); 
		ndarray<T, 2> res(n, k);
		for (typename ndarray<T, 2>::size_type i = 0; i < n; i++) {
			for (typename ndarray<T, 2>::size_type j = 0; j < k; j++) {
				for (typename ndarray<T, 2>::size_type x = 0; x < m; x++) 
					res[i][j] += l[i][x] * r[x][j];
			}
		} 
		return res;
	}

	template <class R = int, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> add(R(), [] (T1 l, T2 r) -> R { return l + r; }); 
	template <class R = int, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> subtract(R(), [] (T1 l, T2 r) -> R { return l - r; }); 
	template <class R = int, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> multiply(R(1), [] (T1 l, T2 r) -> R { return l * r; }); 
	template <class R = int, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> divide(R(1), [] (T1 l, T2 r) -> R { return l / r; }); 
	template <class R = int, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> mod(R(), [] (T1 l, T2 r) -> R { return l % r; }); 
	template <class R = int, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> bitwise_and(~R(), [] (T1 l, T2 r) -> R { return l & r; }); 
	template <class R = int, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> bitwise_or(R(), [] (T1 l, T2 r) -> R { return l | r; }); 
	template <class R = int, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> bitwise_xor(R(), [] (T1 l, T2 r) -> R { return l ^ r; }); 
	template <class R = bool, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> logical_and(!R(), [] (T1 l, T2 r) -> R { return l && r; }); 
	template <class R = bool, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> logical_or(R(), [] (T1 l, T2 r) -> R { return l || r; }); 
	template <class R = int, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> left_shift(R(), [] (T1 l, T2 r) -> R { return l && r; }); 
	template <class R = int, class T1 = const R &, class T2 = T1> ufunc<R(T1, T2)> right_shift(R(), [] (T1 l, T2 r) -> R { return l || r; }); 
	template <class R = int, class T = const R &> ufunc<R(T)> negative([] (T t) -> R { return -t; }); 
	template <class R = int, class T = const R &> ufunc<R(T)> invert([] (T t) -> R { return ~t; }); 

	template <class T, int N> auto sum(const ndarray<T, N> &a) { return add<T>.reduce(a); }
	template <class T> T sum(const ndarray<T, 1> &a, int axis) { return add<T>.reduce(a, axis); }
	template <class T, int N> ndarray<T, N - 1> sum(const ndarray<T, N> &a, int axis) { return add<T>.reduce(a, axis); }
	template <class T, int N> auto cumsum(const ndarray<T, N> &a, int axis = 0) { return add<T>.accumulate(a, axis); }

	template <class T = int, class ...Args> auto empty(Args &&...args) { return ndarray<T, sizeof...(Args)>(forward<Args>(args)...); }
	template <class T, int N> auto empty_like(const ndarray<T, N> &a) { return apply([] (auto &&... args) { return empty<T>(args...); }, a.shape()); }
	template <class T = int, class ...Args> auto full(Args &&...args) { return ndarray<T, sizeof...(Args) - 1>(forward<Args>(args)...); }
	template <class T, int N> auto full_like(const ndarray<T, N> &a, const T &v) { return apply([&v] (auto &&... args) { return full<T>(args..., v); }, a.shape()); }
	template <class T = int, class ...Args> auto zeros(Args &&...args) { return full(forward<Args>(args)..., T(0)); }
	template <class T, int N> auto zeros_like(const ndarray<T, N> &a) { return full_like(a, T(0)); }
	template <class T = int, class ...Args> auto ones(Args &&...args) { return full(forward<Args>(args)..., T(1)); }
	template <class T, int N> auto ones_like(const ndarray<T, N> &a) { return full_like(a, T(1)); }
	template <class T = int> auto identity(size_t n) {
		auto r = zeros(n, n);
		for (size_t i = 0; i < n; i++) r[i][i] = T(1);
		return r;
	}

	template <class T, int N> auto shape(const ndarray<T, N> &a) { return a.shape(); }
	template <class T, int N> auto reshape(const ndarray<T, N> &a) { return a.reshape(); }

}
template <class T1, class T2, int N1, int N2>
auto operator+(const np::ndarray<T1, N1> &t1, const np::ndarray<T2, N2> &t2) { return np::add<decltype(declval<T1>() + declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator-(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::subtract<decltype(declval<T1>() - declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator*(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::multiply<decltype(declval<T1>() * declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator/(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::divide<decltype(declval<T1>() / declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator%(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::mod<decltype(declval<T1>() % declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator&(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::bitwise_and<decltype(declval<T1>() & declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator|(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::bitwise_or<decltype(declval<T1>() | declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator^(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::bitwise_xor<decltype(declval<T1>() ^ declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator&&(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::logical_and<decltype(declval<T1>() && declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator||(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::logical_or<decltype(declval<T1>() || declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator<<(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::left_shift<decltype(declval<T1>() << declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T1, class T2, int N>
auto operator>>(const np::ndarray<T1, N> &t1, const np::ndarray<T2, N> &t2) { return np::right_shift<decltype(declval<T1>() >> declval<T2>()), const T1 &, const T2 &>(t1, t2); }
template <class T, int N>
auto operator-(const np::ndarray<T, N> &t) { return np::negative<decltype(-declval<T>()), const T &>(t); }
template <class T, int N>
auto operator~(const np::ndarray<T, N> &t) { return np::invert<decltype(~declval<T>()), const T &>(t); }

template <class T>
using Mat = np::ndarray<T, 2>;

// }}}